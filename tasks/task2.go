package main

import (
	"fmt"
	"time"
)

func main() {
	dobStr := "01.01.1995"
	givenDate, err := time.Parse("02.01.2006", dobStr)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s is %s", givenDate.Format("02-01-2006"), FindWeekday(givenDate))
}

func FindWeekday(date time.Time) (weekday string) {
	return date.Weekday().String()
}
