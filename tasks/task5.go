package main

import "fmt"

func main() {
	n := 2
	// Read n from input
	DisplayMinimumNumberFunction(n)
}

// https://www.hackerrank.com/contests/w30/challenges/find-the-minimum-number
func DisplayMinimumNumberFunction(n int) {
	var endPar string = ""
	var minint string = ""
	var i int
	for i = 1; i < n; i++ {
		minint += "min(int, "
		endPar += ")"
	}
	fmt.Printf(minint + "int" + endPar)
}
